#ifndef _STDIO_H
#define _STDIO_H 1

#ifdef __cplusplus
extern "C" {
#endif

int printf(const char * const format, ...);
int puts(const char * const str);

#ifdef __cplusplus
}
#endif

#endif
